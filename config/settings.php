<?php

return [
    'main' => [
        'name' => env('APP_NAME'),
        'description' => '',
        'logo' => '',
        'favicon' => '',
        'hero' => '',
    ],
];
