<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Portfolio</title>
    <link rel="stylesheet" href="{{asset('css/styles.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/fontawesome-free/css/all.min.css')}}">
    <style>
        .portfolio {
            display: flex;
            flex-wrap: wrap;
            justify-content: space-around;
            align-items: center;
            margin: 0 auto;
            width: 80%;
        }
        .row-list {
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            margin: auto;
            width: 80%;
        }
        .card-skill {
            /* flex-direction: row;
            width: var(--square-size);
            height: var(--square-size);
            margin: 7px;
            text-align: center;
            background-color: var(--secondary-color);
            box-shadow: var(--third-color) 0px 10px 20px;
            color: var(--fifth-color);
            border-radius: 10px;
            padding: 20px; */
            /* align-content: space-around; */
        }
    </style>
</head>
<body>
    @php
        $skills = \App\Models\Skill::all();
        $words = "Hi, I'm Fabian, web developer";
        // dd(str_split($words));
    @endphp
    <header>
        <nav>
            <div class="content-nav content">
                <div class="logo">
                    {{-- <img src="https://vportfolio.easydelbd.com/uploads/logo.png" alt="logo"> --}}
                    <img src="{{asset('images/logo.svg')}}" alt="logo">
                </div>
                <a href="javascript:void(0);" class="icon fa fa-bars"></a>
                <ul>
                    <li class="active"><a href="#home">Home</a></li>
                    <li><a href="#about">About</a></li>
                    <li><a href="#portfolio">Portfolio</a></li>
                    <li><a href="#contact">Contact</a></li>
                </ul>
            </div>
        </nav>
    </header>
    <main>
        <section id="home" style="">
            <div id="particle"></div>
            <div class="home-content">
                <div class="words">
                    <h1 aria-label="Hi, I'm Fabian,web developer." class="blast-root">
                        Hi, <br> I'm <span class="space"></span>
                        Fabian, <br>
                        web<span class="space"></span>developer.
                    {{-- @foreach (str_split($words) as $letter)
                        <span aria-hidden="true" class="rubberBand">{{$letter}}</span>
                    @endforeach --}}
                    </h1>
                </div>
            </div>
        </section>
        <section id="about">
            <div class="about-area content">
                <div class="title">
                    <h2>About <span>Me</span></h2>
                </div>
                <div class="main-area">
                    <div class="info">
                        <div class="info-description">My name is Fabian Silva, I am a web developer from Guayaquil, Ecuador. I have experience building and customizing websites made in Laravel and other tenologies.</div>
                        <div class="info-content">
                            <h2>Personal Info</h2>
                            <div class="info-item">
                                <h3>Name : <span>Fabian Silva</span></h3>
                            </div>
                            <div class="info-item">
                                <h3>Freelance : <span>Available</span></h3>
                            </div>
                            <div class="info-item">
                                <h3>Location : <span>Guayaquil, Ecuador</span></h3>
                            </div>
                            <div class="info-item">
                                <h3>Skill : <span>Front-End & Back-End</span></h3>
                            </div>
                            <div class="info-item">
                                <h3>Language : <span>Spanish, English</span></h3>
                            </div>
                            <div class="info-item">
                                <h3>Experience : <span>2 Years</span></h3>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <section id="skill">
            <div class="skill-area content">
                <div class="title">
                    <h2><span>My</span> Skills</h2>
                </div>
                <div class="main-area">
                    <div class="items">
                        @foreach ($skills as $skill)
                        <div class="card-skill">
                            <div class="image">
                                <img src="{{$skill->image}}" alt="{{$skill->name}}">
                            </div>
                            <div class="name">
                                {{ Str::upper($skill->name) }}
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
        <section id="portfolio">
            <div class="portfolio-area content">
                <div class="title">
                    <h2><span>My</span> Portfolio</h2>
                </div>
                <div class="main-area">
                    <div class="items">
                        <div class="card-item">
                            <div></div>
                            <div class="card-item-content">
                                {{-- title --}}
                                <h3>BLOG</h3>
                                <ul>
                                    <li>HTML</li>
                                    <li>CSS</li>
                                    <li>VUEJS</li>
                                    <li>LARAVEL</li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-item">
                            <div></div>
                            <div class="card-item-content"></div>
                        </div>
                        <div class="card-item">
                            <div></div>
                            <div class="card-item-content"></div>
                        </div>
                        <div class="card-item">
                            <div></div>
                            <div class="card-item-content"></div>
                        </div>
                        <div class="card-item">
                            <div></div>
                            <div class="card-item-content"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="contact">
            <div class="contact-area content">
                <div class="title">
                    <h2>Contact <span>Me</span></h2>
                </div>
                <div class="main-area">
                    <div class="contact-info">
                        <div class="contact-info-content">
                            <i class="fas fa-mobile-alt"></i>
                            <div class="contact-info-text">
                                <div class="info-title">phone</div>
                                <div class="info-text">{{$user->mobile}}</div>
                            </div>
                        </div>
                        <div class="contact-info-content">
                            <i class="far fa-envelope"></i>
                            <div class="contact-info-text">
                                <div class="info-title">email</div>
                                <div class="info-text">{{$user->email_address}}</div>
                            </div>
                        </div>
                        <div class="contact-info-content">
                            <i class="fas fa-map-marker-alt"></i>
                            <div class="contact-info-text">
                                <div class="info-title">address</div>
                                <div class="info-text">Guayaquil, Ecuador</div>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima autem illo ea necessitatibus, aliquam suscipit, similique minus harum in vel fuga non earum voluptatum quos. Consequatur, minus! Reiciendis, esse itaque.</div> --}}
                    <div id="form"></div>

                </div>
            </div>
        </section>
    </main>
    <footer>
        <div class="socials">
            @foreach($user->socials as $social)
                <a href="{{$social->link}}" target="_blank" class="{{$social->class_name}}"></a>
            @endforeach
            {{-- <a href="https://www.facebook.com/fabian.silva.9" target="_blank" class="fab fa-facebook-f"></a>
            <a href="https://www.instagram.com/fabian_silva_/" target="_blank" class="fab fa-instagram"></a>
            <a href="https://www.linkedin.com/in/fabian-silva-a8a8b5184/" target="_blank" class="fab fa-linkedin-in"></a>
            <a href="https://www.linkedin.com/in/fabian-silva-a8a8b5184/" target="_blank" class="fab fa-github"></a> --}}
        </div>
        <div class="copyright">
            <p>{{ date('Y') }} &copy; All Rights Reserved</p>
        </div>
    </footer>

    {{-- TRABAJOS --}}
    {{-- <h2>Portfolio</h2>
    <div class="portfolio">
        <div class="portfolio__item">
            <img src="{{$skills[0]->image}}" alt="" srcset="">
        </div>
        <div class="portfolio__item">
            <img src="{{asset('images/no-image.png')}}" alt="" srcset="">
        </div>
        <div class="portfolio__item">
            <img src="{{asset('images/no-image.png')}}" alt="" srcset="">
        </div>
    </div> --}}
    {{-- CONTACT --}}
    {{-- <h2>Contacto</h2> --}}
    {{-- script jquery --}}
    <script src="{{ asset('js/jquery.js')}}"></script>
    <script src="{{ asset("js/particles/particles.min.js")}}"></script>
    <script src="{{ asset("js/blast.js")}}"></script>
    <script src="{{ asset("js/frontend.js")}}"></script>
    <script>
        /* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
        particlesJS.load('particle', '{{ asset('js/particles/particles.json') }}', function() {
        console.log('callback - particles.js config loaded');
        });



        // const spans = document.querySelectorAll('.words .letter');
        // spans.forEach((span, idx) => {
        //     span.addEventListener('mouseover', (e) => {
        //         e.target.classList.add('rubberBand');
        //     });
        //     span.addEventListener('animationend', (e) => {
        //         e.target.classList.remove('rubberBand');
        //     });
        // });
    </script>
</body>
</html>
