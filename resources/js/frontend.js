/**
 * Animation blast
 */
$(".words h1").blast({
    delimiter: "character",
    tag: "span",
    customClass: "letter",
});

let a = 0;
$(".words .blast").each(function () {
    if (a == 300) {
        a = 400;
    }

    if (a == 1200) {
        a = 1400;
    }

    var el = $(this);

    if (a == 400) {
        setTimeout(function () {
            $(".words h1 img").addClass("animated rotateIn");
        }, 500);
    }

    setTimeout(function () {
        el.addClass("animated bounceIn");
    }, a);

    if (a < 1200) {
        a = a + 100;
    } else {
        a = a + 70;
    }
});
setTimeout(function () {
    $(".words .blast").removeClass("animated bounceIn");
    $(".words .blast").css("opacity", 1);

    $(".words .blast").mouseenter(function () {
        var el = $(this);

        $(this).addClass("animated rubberBand");
        $(this).one(
            "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend",
            function () {
                el.removeClass("animated rubberBand");
            }
        );
    });
}, 3000);

setTimeout(function () {
    $(".words .flat-button").addClass("animated bounceIn");
    $(".words .flat-button").one(
        "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend",
        function () {
            $(".words .flat-button").removeClass("animated bounceIn");
            $(".words .flat-button").css("opacity", 1);
        }
    );
}, 2000);

$(".words .flat-button").mouseenter(function () {
    var el = $(this);

    $(this).addClass("animated rubberBand");
    $(this).one(
        "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend",
        function () {
            el.removeClass("animated rubberBand");
        }
    );
});

const navbar = document.querySelector("nav");
const sections = document.querySelectorAll("section");
const navbarLinks = navbar.querySelectorAll("li");

window.onscroll = () => {
    let currentSection = "";

    if (window.scrollY > 330) {
        navbar.classList.add("navbar-dark");
    } else {
        navbar.classList.remove("navbar-dark");
    }

    // console.log(pageYOffset);
    sections.forEach((section, idx) => {
        // console.log("section",section.offsetTop);
        if (scrollY >= section.offsetTop - 60) {
            currentSection = section.getAttribute("id");
        }
    });

    navbarLinks.forEach((li) => {
        li.classList.remove("active");
        if (li.querySelector("a").href.includes(currentSection)) {
            li.classList.add("active");
        }
    });
};

document.addEventListener("click", (e) => {
    if (e.target.matches("nav li") || e.target.matches("nav li a")) {
        //  Sections
        e.preventDefault();
        const elementA =
            e.target.tagName === "LI"
                ? e.target.querySelector("a").getAttribute("href")
                : e.target.getAttribute("href");

        const section = document.querySelector(elementA);
        const sectionTop = section.offsetTop - 60;

        window.scrollTo({
            top: sectionTop,
            behavior: "smooth",
        });
    } else if (e.target.matches("nav .icon")) {
        //  Navbar responsive
        document.querySelector("nav").classList.toggle("navbar-responsive");
    }
});

// const formContact = document.querySelector(".form-contact");

/*formContact.addEventListener("submit", async (e) => {
    e.preventDefault();
    e.stopPropagation();
    console.log("Contact form submitted");

    // sendMail();
    // const formContact = document.querySelector(".form-contact");
    const name = formContact.querySelector("#name").value;
    const email = formContact.querySelector("#email").value;
    const subject = formContact.querySelector("#subject").value;
    const message = formContact.querySelector("#message").value;
    // const urllocal = window.location.href;
    try {
        const response = await axios.post(
            "/api/contact",
            {
                name,
                email,
                subject,
                message,
            },
            {
                headers: {
                    "Content-Type": "application/json",
                },
            }
        );

        console.log(response.data);
    } catch (error) {
        console.error(error);
    }
    e.preventDefault();
    e.stopPropagation();
    console.log("End form");
});

async function sendMail() {
    // fetch("/api/contact", {
    //     method: "POST",
    //     headers: {
    //         "Content-Type": "application/json",
    //     },
    //     body: JSON.stringify({
    //         name,
    //         email,
    //         subject,
    //         message,
    //     }),
    // })
    //     .then((res) => {
    //         // console.log(res);
    //     })
    //     .catch((err) => {
    //         console.log(err);
    //     });
}*/
require("./bootstrap");
import { createApp } from "vue";
import Form from "./components/FormContact.vue";
createApp(Form)
    // .use(store)
    .mount("#form");
