import { createRouter, createWebHistory } from "vue-router";

import Tagline from "../components/pages/TaglinePage.vue";
import About from "../components/pages/AboutPage.vue";
import Category from "../components/pages/CategoryPage.vue";
import Portfolio from "../components/pages/PortfolioPage.vue";
import Service from "../components/pages/ServicePage.vue";
import Skill from "../components/pages/SkillPage.vue";
import Social from "../components/pages/SocialPage.vue";
import Setting from "../components/pages/SettingPage.vue";
import Profile from "../components/pages/ProfilePage.vue";
import Password from "../components/pages/PasswordPage.vue";

// import store from "../store";

const routes = [
    {
        path: "/admin/tagline",
        name: "Tagline",
        component: Tagline,
        // meta: { requiresAuth: true },
    },
    {
        path: "/admin/about",
        name: "About",
        component: About,
        // meta: { requiresAuth: true },
    },
    {
        path: "/admin/category",
        name: "Category",
        component: Category,
    },
    {
        path: "/admin/portfolio",
        name: "Portfolio",
        component: Portfolio,
    },
    {
        path: "/admin/service",
        name: "Service",
        component: Service,
    },
    {
        path: "/admin/skill",
        name: "Skill",
        component: Skill,
    },
    {
        path: "/admin/social",
        name: "Social",
        component: Social,
    },
    {
        path: "/admin/settings",
        name: "Setting",
        component: Setting,
    },
    {
        path: "/admin/profile",
        name: "Profile",
        component: Profile,
    },
    {
        path: "/admin/change-password",
        name: "Password",
        component: Password,
    },
];

// {
//     path: "/:pathMatch(.*)*",
//     component: notFound,
// },

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
});

// router.beforeEach(async (to, from, next) => {
//     await store.dispatch("getUser");
//     console.log("beforeEach", from, to);
//     if (to.meta.requiresAuth && !store.state.auth) {
//         next({ name: "Login" });
//     } else if (to.name == "Login" && store.state.auth) {
//         next({ name: "Home" });
//     } else {
//         next();
//     }
// });

export default router;
