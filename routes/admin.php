<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('dashboard', 'admin.app', ['page' => 'Dashboard']);

Route::view('tagline', 'admin.app', ['page' => 'Tagline']);

Route::view('about', 'admin.app', ['page' => 'About']);

Route::view('skill', 'admin.app', ['page' => 'Skills']);

Route::view('service', 'admin.app', ['page' => 'Service']);

Route::view('category', 'admin.app', ['page' => 'Categories']);

Route::view('portfolio', 'admin.app', ['page' => 'Portfolio']);

Route::view('social', 'admin.app', ['page' => 'Social Link']);

Route::view('settings', 'admin.app', ['page' => 'Settings']);

Route::view('profile', 'admin.app', ['page' => 'Setting Profile']);

Route::view('change-password', 'admin.app', ['page' => 'Change Password']);
