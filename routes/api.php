<?php

use App\Http\Controllers\AboutController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PortfolioController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\SkillController;
use App\Http\Controllers\SocialController;
use App\Http\Controllers\TaglineController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('private-info', function () {
    return response()->json([
        'message' => 'This is a private message!',
    ]);
})->middleware('auth:sanctum');

Route::resource('taglines', TaglineController::class)->middleware('auth:sanctum');
Route::resource('categories', CategoryController::class)->middleware('auth:sanctum');
Route::resource('socials', SocialController::class)->middleware('auth:sanctum');

Route::resource('skills', SkillController::class)->middleware('auth:sanctum');
Route::post('skills/update/{skill}', [SkillController::class, 'update'])->middleware('auth:sanctum');

Route::resource('services', ServiceController::class)->middleware('auth:sanctum');
Route::post('services/update/{id}', [ServiceController::class, 'update'])->middleware('auth:sanctum');

Route::resource('abouts', AboutController::class)->middleware('auth:sanctum');
Route::post('abouts/update/{about}', [AboutController::class, 'update'])->middleware('auth:sanctum');

Route::resource('portfolios', PortfolioController::class)->middleware('auth:sanctum');
Route::post('portfolios/update/{portfolio}', [PortfolioController::class, 'update'])->middleware('auth:sanctum');

Route::get('settings', [SettingController::class, 'show'])->middleware('auth:sanctum');
Route::post('settings', [SettingController::class, 'update'])->middleware('auth:sanctum');

Route::get('profile', [UserController::class, 'show'])->middleware('auth:sanctum');
Route::post('profile', [UserController::class, 'update'])->middleware('auth:sanctum');

Route::post('change-password', [UserController::class, 'changePassword'])->middleware('auth:sanctum');

Route::post('contact', [UserController::class, 'contact'])->middleware('auth:sanctum');
