<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    public function show()
    {
        $user = Auth::user();
        return response()->json(['user' => $user], 200);
    }

    public function update(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $avatar = $user->avatar;
        $user->update($request->all());

        $image_path = public_path('images/avatar/');

        if ($request->file('avatar')) {
            if(file_exists($image_path . $avatar)) {
                unlink($image_path . $avatar);
            }

            $file = $request->file('avatar');
            $name = $user->id.'_'.time().'.'.$file->getClientOriginalExtension();
            $file->move($image_path, $name);

            $user->avatar = $name;
            $user->save();

            return response()->json([
                'message' => 'File detectado!',
                'user' => $user,
                'file'  => $request->file('avatar'),
                'full_path' => $image_path
            ]);
        }

        return response()->json([
            'message' => 'User updated successfully!',
            'user' => $user,
            'request' => $request
        ], 200);
    }

    public function changePassword(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $credentials = $request->validate([
            'password' => ['required', 'string'],
            'new_password' => ['required', 'string'],
            'repeat_password' => ['required', 'string'],
        ]);

        if ($credentials['new_password'] !== $credentials['repeat_password']) {
            return response()->json([
                'message' => 'New password and repeat password does not match!'
            ], 400);
        }
        // if (Hash::check($request->password, $user->password)) {
        if (!Hash::check($credentials['password'], $user->password)) {
            return response()->json([
                'message' => 'Current password does not match!'
            ], 400);
        }

        $user->password = bcrypt($request->new_password);
        $user->save();

        return response()->json([
            'message' => 'Password updated successfully!',
            'user' => $user
        ], 200);
    }

    public function contact(Request $request)
    {
        // name, email, subject, message
        $request->validate([
            'name' => ['required', 'string'],
            'email' => ['required', 'email'],
            'subject' => ['required', 'string'],
            'message' => ['string'],
        ]);

        Mail::to('devawsoftware@gmail.com')->send(new ContactMail($request->all()));

        return response()->json([
            'message' => 'Email sent successfully!',
            'request' => $request
        ]);
    }
}
