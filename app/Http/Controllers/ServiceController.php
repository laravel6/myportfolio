<?php

namespace App\Http\Controllers;

use App\Models\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::all();

        return response()->json([
            'services' => $services,
            'message' => 'Successfully retrieved services!',
            'files' => file_exists(public_path('images/service/mp_1651969662.png'))
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $service = new Service($request->all());
        $service->user_id = auth()->user()->id;

        if ($request->file('icon')) {
            $file = $request->file('icon');
            $name = 'mp_'.time().'.'.$file->getClientOriginalExtension();
            $path = public_path().'/images/service/';
            $file->move($path, $name);

            $service->icon = $name;
        }

        $service->save();

        return response()->json([
            'message' => 'Service created successfully!',
            'service' => $service,
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service = Service::find($id);
        $service->preventAttrSet = true;
        $full_path = public_path('images/service/').$service->icon;

        // return response()->json(['request' => $request, 'service' => $service]);

        $service->update($request->all());

        // return response()->json([
        //     'icon' => $service->icon,
        //     // getAttribute('icon'),
        //     'message' => 'Service updated successfully!',
        //     'service' => $service,
        // ]);

        if ($request->file('icon')) {
            if(file_exists($full_path)) {
                unlink($full_path);
            }

            $file = $request->file('icon');
            $name = 'mp_'.time().'.'.$file->getClientOriginalExtension();
            $path = public_path().'/images/service/';
            $file->move($path, $name);

            $service->icon = $name;
            $service->save();

            return response()->json([
                'message' => 'File detectado!',
                'service' => $service,
                'file'  => $request->file('icon'),
                'full_path' => $full_path
            ]);
        }

        return response()->json([
            'message' => 'Service updated successfully!',
            'service' => $service,
            'request' => $request
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        if (file_exists($service->icon)) {
            unlink($service->icon);
        }
        $service->delete();
    }
}
