<?php

namespace App\Http\Controllers;

use App\Models\Skill;
use Illuminate\Http\Request;

class SkillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $skills = Skill::all();

        return response()->json([
            'skills' => $skills,
            'message' => 'Successfully retrieved skills!',
            // 'files' => file_exists(public_path('images/service/mp_1651969662.png'))
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $skill = new Skill($request->all());

        if ($request->file('image')) {
            $file = $request->file('image');
            $name = 'mp_'.time().'.'.$file->getClientOriginalExtension();
            $path = public_path().'/images/skill/';
            $file->move($path, $name);

            $skill->image = $name;
        }

        $skill->save();

        return response()->json([
            'message' => 'Skill created successfully!',
            'skill' => $skill,
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Skill  $skill
     * @return \Illuminate\Http\Response
     */
    public function show(Skill $skill)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Skill  $skill
     * @return \Illuminate\Http\Response
     */
    public function edit(Skill $skill)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Skill  $skill
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Skill $skill)
    {
        // $skill = Skill::find($id);
        $skill->preventAttrSet = true;
        $full_path = public_path('images/skill/').$skill->image;

        $skill->update($request->all());

        if ($request->file('image')) {
            if(file_exists($full_path)) {
                unlink($full_path);
            }

            $file = $request->file('image');
            $name = 'mp_'.time().'.'.$file->getClientOriginalExtension();
            $path = public_path().'/images/skill/';
            $file->move($path, $name);

            $skill->image = $name;
            $skill->save();

            return response()->json([
                'message' => 'File detectado!',
                'skill' => $skill,
                'file'  => $request->file('image'),
                'full_path' => $full_path
            ]);
        }

        return response()->json([
            'message' => 'Skill updated successfully!',
            'skill' => $skill,
            'request' => $request
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Skill  $skill
     * @return \Illuminate\Http\Response
     */
    public function destroy(Skill $skill)
    {
        $full_path = public_path('images/skill/').$skill->image;
        if (file_exists($full_path)) {
            unlink($full_path);
        }
        $skill->delete();
    }
}
