<?php

namespace App\Http\Controllers;

use App\Models\Tagline;
use Illuminate\Http\Request;

class TaglineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $taglines = Tagline::all()->where('user_id', auth()->user()->id);
        return response()->json(['taglines' => $taglines], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tagline = new Tagline($request->all());
        $tagline->user_id = auth()->user()->id;
        $tagline->save();

        return response()->json(['tagline' => $tagline]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tagline  $tagline
     * @return \Illuminate\Http\Response
     */
    public function show(Tagline $tagline)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tagline  $tagline
     * @return \Illuminate\Http\Response
     */
    public function edit(Tagline $tagline)
    {
        return response()->json(['tagline' => $tagline]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tagline  $tagline
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tagline $tagline)
    {
        $tagline->update($request->all());
        return response()->json(['tagline' => $tagline]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tagline  $tagline
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tagline $tagline)
    {
        $tagline->delete();
    }
}
