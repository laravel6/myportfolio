<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class SettingController extends Controller
{
    public function show()
    {
        $settings = [
            'name' => config('settings.main.name'),
            'description' => config('settings.main.description'),
            'logo' => config('settings.main.logo'),
            'favicon' => config('settings.main.favicon'),
            'hero' => config('settings.main.hero'),
        ];

        return response()->json(['settings' => $settings], 200);
    }

    public function update(Request $request)
    {
        $settings = $request->all();
        // return response()->json(['settings' => $settings]);

        foreach ($settings as $key => $value) {
            Setting::updateOrCreate(['key' => 'main.'.$key], ['value' => $value]);
        }


        foreach (['logo', 'favicon', 'hero'] as $image) {
            $image_path = $image == 'favicon' ? public_path('') : public_path('images/');
            if ($request->file($image)) {
                $file = $request->file($image);
                $extension = $image == 'favicon' ? 'ico': $file->getClientOriginalExtension();
                $name = $image.'.'.$extension;

                if(file_exists($image_path . $name)) {
                    unlink($image_path . $name);
                }

                // $path = public_path().'/images/';
                $file->move($image_path, $name);

                Setting::updateOrCreate(['key' => 'main.'.$image], ['value' => $name]);
            }
        }


        Artisan::call('settings:load-file');
    }
}
