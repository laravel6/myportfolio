<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Portfolio;
use App\Models\Skill;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portfolios = Portfolio::all()->where('user_id', auth()->user()->id);
        $categories = Category::all();
        $skills = Skill::all();

        foreach ($portfolios as $key => $value) {
            $portfolios[$key]['skills'] = Portfolio::find($value->id)->skills->pluck('id')->toArray();
        }

        return response()->json([
            'message' => 'Successfully retrieved portfolio!',
            'portfolios' => $portfolios,
            'categories' => $categories,
            'skills' => $skills,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $portfolio = new Portfolio($request->all());
        $portfolio->user_id = auth()->user()->id;

        if ($request->file('image')) {
            $file = $request->file('image');
            $name = 'mp_'.time().'.'.$file->getClientOriginalExtension();
            $path = public_path().'/images/portfolio/';
            $file->move($path, $name);

            $portfolio->image = $name;
        }

        $portfolio->save();
        $portfolio->skills()->sync($request->skills);

        return response()->json([
            'message' => 'Skill created successfully!',
            'portfolio' => $portfolio,
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function show(Portfolio $portfolio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function edit(Portfolio $portfolio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Portfolio $portfolio)
    {
        $portfolio = Portfolio::find($portfolio->id);
        $portfolio->preventAttrSet = true;
        $full_path = public_path('images/skill/').$portfolio->image;

        $portfolio->skills()->sync(explode(",",$request->skills));
        $portfolio->update($request->all());

        if ($request->file('image')) {
            if(file_exists($full_path) && $portfolio->image) {
                unlink($full_path);
            }

            $file = $request->file('image');
            $name = 'mp_'.time().'.'.$file->getClientOriginalExtension();
            $path = public_path().'/images/portfolio/';
            $file->move($path, $name);

            $portfolio->image = $name;
            $portfolio->save();
        }

        return response()->json([
            'message' => 'Portfolio updated successfully!',
            'portfolio' => $portfolio,
            'request' => $request
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Portfolio $portfolio)
    {
        $full_path = public_path('images/portfolio/').$portfolio->image;

        if (file_exists($full_path) && $portfolio->image) {
            unlink($full_path);
        }
        $portfolio->delete();
    }
}
