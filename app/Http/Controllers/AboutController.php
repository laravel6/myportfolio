<?php

namespace App\Http\Controllers;

use App\Models\About;
use App\Models\User;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\About  $about
     * @return \Illuminate\Http\Response
     */
    public function show($id = null)
    {
        $user = User::find(auth()->user()->id);
        $about = About::where('user_id', $user->id)->first();

        if ($about == null) {
            $about = new About();
            $about->description = "";
            $about->user_id = $user->id;
            $about->save();
        }

        return response()->json([
            'message' => 'success',
            'about' => $about
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\About  $about
     * @return \Illuminate\Http\Response
     */
    public function edit(About $about)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\About  $about
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, About $about)
    {
        $about->preventAttrSet = true;
        $image_path = public_path('images/about/').$about->image;
        $cv_path = public_path('files/').$about->cv;

        $about->update($request->all());

        if ($request->file('image')) {
            if(file_exists($image_path) && $about->image) {
                unlink($image_path);
            }

            $file = $request->file('image');
            $name = 'mp_'.time().'.'.$file->getClientOriginalExtension();
            $path = public_path().'/images/about/';
            $file->move($path, $name);

            $about->image = $name;
            $about->save();
        }

        if ($request->file('cv') && $about->cv) {
            if(file_exists($cv_path)) {
                unlink($cv_path);
            }

            $file = $request->file('cv');
            $name = 'mp_'.time().'.'.$file->getClientOriginalExtension();
            $path = public_path().'/files/';
            $file->move($path, $name);

            $about->cv = $name;
            $about->save();

            return response()->json([
                'message' => 'File detectado!',
                'about' => $about,
                'request' => $request,
                'file' => $request->file('cv'),
            ]);
        }

        return response()->json([
            'message' => 'About updated successfully!',
            'about' => $about,
            'request' => $request
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\About  $about
     * @return \Illuminate\Http\Response
     */
    public function destroy(About $about)
    {
        //
    }
}
