<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    use HasFactory;

    protected $table = "abouts";
    protected $fillable = ['description', 'cv', 'image', 'user_id'];
    public $preventAttrSet = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function image(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $this->preventAttrSet ? $value : ($value ? asset('images/about/' . $value) : asset('images/no-image.png')),
        );
    }

    public function cv(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $this->preventAttrSet ? $value : ($value ? asset('files/' . $value) : ''),
        );
    }
}
