<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    use HasFactory;

    protected $table = "portfolios";
    protected $fillable = ['title', 'link', 'image', 'target', 'category_id', 'user_id'];
    public $preventAttrSet = false;

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function skills()
    {
        return $this->belongsToMany(Skill::class);
    }

    public function image(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $this->preventAttrSet ? $value : ($value ? asset('images/portfolio/' . $value) : asset('images/no-image.png')),
        );
    }
}
