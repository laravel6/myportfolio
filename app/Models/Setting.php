<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class Setting extends Model
{
    use HasFactory;

    protected $fillable = ['key', 'value'];

    public $timestamps = false;

    public static function chargeConfig()
    {
        $settings = Cache::get('db_setting', []);

        // $settings = collect(require config_path('settings.php'))->each(fn($value, $key) => app('config')->set(['setting.' . $key => $value]));
        if ($settings instanceof Collection) {
            collect($settings)->each(fn($setting) => app('config')->set(['settings.' . $setting->key => $setting->value]));
        }

        config(['app.name' => config('settings.main.name')]);
        config([
            'adminlte.title' => config('settings.main.name'),
            'adminlte.title_postfix' => ' | '.config('settings.main.name'),
            'adminlte.logo' => '<b>'.config('settings.main.name').'</b>',
            // 'adminlte.logo_img_alt' => config('settings.main.name'),
        ]);
        // config(['adminlte.title' => config('settings.main.name')]);
        // config(['adminlte.title_postfix' => ' | '.config('settings.main.name')]);
        // config(['logo' => '<b>'.env('APP_NAME').'</b>']);

        return $settings;
    }

    public static function refreshCache()
    {
        Cache::forget('db_setting');
        Cache::rememberForever('db_setting', fn() => static::query()->get()->toBase());

        return self::chargeConfig();
    }
}
