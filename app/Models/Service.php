<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Service extends Model
{
    use HasFactory;

    protected $table = "services";
    protected $fillable = ['title', 'description', 'icon', 'user_id', 'image'];
    public $preventAttrSet = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function icon(): Attribute {
        return Attribute::make(get: fn($value) =>
            $this->preventAttrSet ? $value : ($value ? asset('images/service/' . $value) : asset('images/no-image.png'))
        );
    }
}
