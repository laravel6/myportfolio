<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    use HasFactory;

    protected $table = "skills";
    protected $fillable = ['name', 'percent', 'image'];
    public $preventAttrSet = false;

    public function portfolio() {
        return $this->belongsToMany(Portfolio::class)->withTimestamps();
    }

    public function image(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $this->preventAttrSet ? $value : ($value ? asset('images/skill/' . $value) : asset('images/no-image.png')),
        );
    }
}
