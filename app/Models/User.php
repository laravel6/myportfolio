<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'avatar',
        'description',
        'mobile',
        'email_address'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public $preventAttrSet = false;

    public function portfolios()
    {
        return $this->hasMany(Portfolio::class);
    }

    public function about()
    {
        return $this->hasOne(About::class);
    }

    public function avatar(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $this->preventAttrSet ? $value : ($value ? asset('images/avatar/' . $value) : asset('images/no-image.png')),
        );
    }

    public function socials()
    {
        return $this->hasMany(Social::class);
    }
}
